class GooglePage {
    get searchtextarea () { return $('[name=q]') }
    get searchbutton () { return $('[name=btnK]') }

    getPageTitle () {
        return browser.getPageTitle()
    }

    doSearchGoogle (searchText) {
        this.searchtextarea.waitForEnabled()
        this.searchtextarea.setValue(searchText)
        this.searchbutton.click()
    }

    doSearchGoogleTwo (searchText, searchText2) {
        this.searchtextarea.setValue(searchText)
        this.searchbutton.click()
    }
}

module.exports = new GooglePage()