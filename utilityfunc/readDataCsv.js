const fs = require('fs')
const parse = require('csv-parse/lib/sync')

function readDataCSV (fileName, rowNumber) {
    const content = fs.readFileSync(fileName)
    const records = parse(content, {
        columns: true,
        skip_empty_lines: true,
        Headers: true
    })
    for( i = 0; i < records.length; i++ ) {
        if (records[i].SNO = rowNumber) {
            return records[i]
        }
    }
}

module.exports = {readDataCSV}
