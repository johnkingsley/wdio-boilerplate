const searchTestData = require("../../data/basicSearchTest.data.js")

describe("Search Google",function() {
    it("Enter Search Value in Google using Parameterized data", function() {
        browser.url('/');
        const search = $('[name=q]');
        search.setValue(searchTestData.searchString);
        const searchBtn = $('[name=btnK]');
        searchBtn.click();
        expect(browser).toHaveTitle('Hello World - Google Search');
    });
});