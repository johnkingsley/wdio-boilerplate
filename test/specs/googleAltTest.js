const googlePage = require('../../pageobjects/google.page.js');
const readDataCsv = require('../../utilityfunc/readDataCsv.js')

describe("Search Google",function() {
    xit("Enter Search Value in Google Page Objects", function() {
        browser.url('/');
        googlePage.doSearchGoogle("Hello World");
        expect(browser).toHaveTitle('Hello World - Google Search');
    });
    it("Enter Search Value in Google Page Objects & CSV data", function() {
        let testData = readDataCsv.readDataCSV('./data/googleDataCSV.csv', '1');
        browser.url('/')
        googlePage.doSearchGoogle(testData.SearchText)
        expect(browser).toHaveTitle(testData.ExpectedValue)
    });
});